// this is the dice
const dice = [
    1,
    2,
    3,
    4,
    5,
    6
];

// set the total player and chance to roll the dice
const player = 4;
const chanceToRolllDice = 3;

// array to store the each dice that will give to each player
const playerDice = [];

// lets rock and roll
for (let index = 1; index <= chanceToRolllDice; index++) {
    console.log('-------------------')
    for (let playerTurn = 1; playerTurn <= player; playerTurn++) {
        // the logic was being here
        if (playerDice.length < player) {

            let rollDice = rollDiceForEachPlayer(chanceToRolllDice, dice);
            let playerData = {
                no: playerTurn,
                dices: rollDice.dices,
                score: rollDice.score,
                turn: rollDice.range,
                friendChance: rollDice.chanceForFriend
            };
            playerDice.push(playerData);

            if (playerDice.length == player) {
                playerDice.map((item) => {
                    if (item.no === playerDice.slice(-1).pop().no) {
                        playerDice[0].turn += item.friendChance;
                    } else {
                        playerDice[item.no].turn += item.friendChance
                    }
                });
            }

        } else {
            playerDice.map((item) => {
                if (item.no == playerTurn) {
                    let rollDice = rollDiceForEachPlayer(item.turn, dice);
                    item.dices = rollDice.dices;
                    item.score += rollDice.score;
                    item.turn = rollDice.range;
                    item.friendChance = rollDice.chanceForFriend;

                    if (item.no === playerDice.slice(-1).pop().no) {
                        playerDice[0].turn += item.friendChance;
                    } else {
                        playerDice[item.no].turn += item.friendChance;
                    }
                }
            });
        }

    }
    // print the result
    playerDice.forEach((el) => {
        console.log({player_no: el.no, dices: el.dices, score: el.score})
    });
}

// function for roll the dice according to the chance
function rollDiceForEachPlayer(range, dice) {
    getDice = [];
    defRange = range;
    getScore = 0;
    giveFriend = 0;
    for (let rollEachPlayer = 1; rollEachPlayer <= range; rollEachPlayer++) {
        let randomDice = dice[Math.floor(Math.random() * dice.length)];
        if (randomDice == 6) {
            getScore += 1;
            defRange -= 1;
        } else if (randomDice == 1) {
            defRange -= 1;
            giveFriend += 1;
            getDice.push(randomDice);
        } else {
            getDice.push(randomDice);
        }
    }
    return {dices: getDice, range: defRange, score: getScore, chanceForFriend: giveFriend};
}
